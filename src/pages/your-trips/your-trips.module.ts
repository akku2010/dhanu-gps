import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YourTripsPage } from './your-trips';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';

@NgModule({
  declarations: [
    YourTripsPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(YourTripsPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
  exports: [
    OnCreate
  ],
})
export class YourTripsPageModule {}
