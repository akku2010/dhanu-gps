import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GeofenceReportPage } from './geofence-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    GeofenceReportPage,
  ],
  imports: [
    IonicPageModule.forChild(GeofenceReportPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class GeofenceReportPageModule {}
